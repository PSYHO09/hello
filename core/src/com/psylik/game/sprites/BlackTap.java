package com.psylik.game.sprites;

/**
 * Created by PSYHO on 21.03.2018.
 */


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Rectangle;


import java.util.Random;

public class BlackTap {
    public static final int GRAVITY = -15;
    private Vector3 position;
    private Vector3 velosity;
    private Random random;
    private static final int[] mass = {0,120,240,359};

    private Texture blacktap;

    public BlackTap(int x, int y){
        random = new Random();
        position = new Vector3(mass[random.nextInt(4)], 1000, 0);
        velosity = new Vector3(0, 0, 0);
        blacktap = new Texture("blacktap.png");


    }

     public int getWidth(){
        return blacktap.getWidth();
     }

     public int getHeight(){
         return blacktap.getHeight();
     }


    public Vector3 getPosition() {
        return position;
    }


    public Texture getBlacktap() {
        return blacktap;
    }



    public void update(float dt){
        if (position.y > 0)
            velosity.add(0, GRAVITY, 0);
        velosity.scl(dt);
        position.add(0, velosity.y, 0);
        if (position.y < 0)
            position.y = 0;

        velosity.scl(1 / dt);



    }




        public void touch(){
        blacktap.dispose();
        }




}
