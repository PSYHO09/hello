package com.psylik.game.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.psylik.game.MyGame;
import com.psylik.game.sprites.BlackTap;
import com.badlogic.gdx.utils.Array;

/**
 * Created by PSYHO on 21.03.2018.
 */

public class PlayState extends State {

    private BlackTap blackTap;
    private Texture bg;

    Vector3 touchPos = new Vector3();




    public PlayState(GameStateManager gsm) {
        super(gsm);
        blackTap = new BlackTap(120, 200);

        camera.setToOrtho(false, MyGame.WIDTH , MyGame.HEIGHT );
        bg = new Texture("bg.png");


    }

    @Override
    protected void handleInput() {
      if(Gdx.input.isTouched()) {
           touchPos.set(Gdx.input.getX(), Gdx.input.getY(),0);
          camera.unproject(touchPos);
          if(blackTap.getPosition().x < touchPos.x && blackTap.getPosition().y + blackTap.getWidth() > touchPos.y){
              if(touchPos.y> blackTap.getPosition().y && touchPos.y < blackTap.getPosition().y + blackTap.getHeight()) {
                  blackTap.touch();
                  blackTap = new BlackTap(120, 200);
              }
          }
      }

        }





    @Override
    public void update(float dt) {
        blackTap.update(dt);
        handleInput();




    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(bg, camera.position.x - (camera.viewportWidth / 2), 0);
        sb.draw(blackTap.getBlacktap(), blackTap.getPosition().x, blackTap.getPosition().y);

        sb.end();

    }

    @Override
    public void dispose() {

    }
}
